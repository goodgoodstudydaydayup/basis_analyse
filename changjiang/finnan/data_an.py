import numpy as np
import pandas as pd
from pandas import DataFrame,Series
# np.set_printoptions(suppress=True)
# pd.set_option('display.max_rows', None)

def flattenlist(x):
    x = np.array(x)
    x = x.flatten()
    return x

def listflatten(x):
    x = np.array(x)
    x = x.flatten()
    return list(x)

def findstations(station, staryear, endyear, starmonth, endmonth):
    sta = []
    year = []
    month = [9, 10]*(endyear+1-staryear)
    for i in station:
        sta.append([i] * (57*2))
    for i in range(staryear, endyear+1):
        year.append([i]*2)

    year = year*198
    month = month*198
    sta = flattenlist(sta)
    year = flattenlist(year)
    month = flattenlist(month)
    return sta, year, month

def isleap(year):
    if ((year%4==0) and (year%100!=0)) or year%400==0:
        return 1
    else:
        return 0

def daystation(station, staryear, endyear):
    arnm = []
    anm = []
    rnm = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    nm = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    rmonth = []
    smonth = []
    for i in range(12):
        arnm.extend(list(range(1, rnm[i]+1)))
        rmonth.extend([i+1] * rnm[i])
        anm.extend(list(range(1, nm[i]+1)))
        smonth.extend([i+1] * nm[i])

    nsta = len(station)
    nyear = endyear+1-staryear
    sta = []
    year = []
    month = []
    day = []
    for i in range(staryear, endyear + 1):
        if isleap(i) == 1:
            day.extend(arnm)
            month.extend(rmonth)
            year.extend([i] * 366)
        else:
            day.extend(anm)
            month.extend(smonth)
            year.extend([i] * 365)
    for i in station:
        sta.extend([i] * len(year))
    day = day * nsta
    month = month * nsta
    year = year * nsta
    return np.array(sta).flatten(), np.array(year), np.array(month), np.array(day)


def allfindstations(station, staryear, endyear ,month=None):
    nsta = len(station)
    nyear = endyear+1-staryear
    sta = []
    year = []
    if month == None:
        for i in station:
            sta.append([i] * nyear)
        for i in range(staryear, endyear+1):
            year.append([i])
        year = year * nsta
    else:
        nmonth = len(month)
        month = month * nyear
        for i in station:
            sta.append([i] * (nyear * nmonth))
        for i in range(staryear, endyear+1):
            year.append([i] * nmonth)
        year = year * nsta
        month = month * nsta
        month = flattenlist(month)

    sta = flattenlist(sta)
    year = flattenlist(year)

    return sta, year, month
def indexname(station, year, month=None, day=None):
    name = []
    month[np.where(month[:] == 1)] = 99
    for i in range(len(station)):
    #     if month == None:
    #         name.extend('%s' % (str(int(station[i])) + str(year[i])))
    #     else:
    #         name.extend('%s'%(str(int(station[i]))+str(year[i])+str(month[i])))
    #     if day !=None:
        name.append('%s' % (str(int(station[i])) + str(int(year[i])) + str(int(month[i])) + str(int(day[i]))))
    return name

# rowdata = pd.read_csv(r'data.csv', index_col=[0])
# station = np.array(pd.read_csv(r'geoshp.csv', usecols=[0], index_col=None))
# # print(len(rowdata.index.unique()), len(station.index.unique()))
# sta, year, month = allfindstations(station, 1980, 2016)
# findnan = DataFrame(year, index=sta, columns=['year'])
#
# per = np.array([0] * len(sta))
# pernan = Series(per.flatten(), index=indexname(sta, year))
# perrowindex = indexname(np.array(rowdata.index), np.array(rowdata['year']))
# perrow = Series(np.array(rowdata['pre']), index=perrowindex)
# fine = pernan+perrow
# # print(pernan, perrow)
# findnan['pre'] = np.array(fine)
#
# findnan.to_csv('try.csv')

# rowdata = pd.read_csv(r'rowdata.csv', index_col=[0])
# station = np.array(pd.read_csv(r'geoshp.csv', usecols=[0], index_col=None))
# sta, year, month, day = daystation(station, 1980, 2016)
#
# findnan = DataFrame(year, index=sta, columns=['year'])
# findnan['month'] = month
# findnan['day'] = day
# per = np.array([0] * len(sta))
# pernanindex = indexname(sta, year, month, day)
# # np.savetxt('pernanindex.txt', pernanindex, fmt='%s')
#
# pernan = Series(per.flatten(), index=pernanindex)
#
# perrowindex = indexname(np.array(rowdata.index), np.array(rowdata['year']), np.array(rowdata['month']), np.array(rowdata['day']))
# perrow = Series(np.array(rowdata['pre']), index=perrowindex)
#
# # np.savetxt('perrowindex.txt', perrowindex, fmt='%s')
# fine = pernan+perrow
# fine = fine.reindex(pernan.index)
# findnan['pre'] = np.array(fine)
# findnan.to_csv('try.csv')


# df1 = pd.read_csv('pernanindex.txt', index_col=0, header=None)
# # df2 = pd.read_csv('perrowindex.txt', index_col=0, header=None)
# # df3 = pd.read_csv('fine.csv', index_col=0, header=None)
# # print(df2.shape, df3.shape)
# print(df1.index[df1.index.duplicated(keep='first')])

# data = pd.read_csv('try.csv', index_col=0)
# geosta = pd.read_csv('geoshp.csv', index_col=0)
# nanindex = (data[np.isnan(data['pre'])].index).unique()
# for i in nanindex:
#     data = data.drop(i)
#     geosta = geosta.drop(i)
# data.to_csv('nonanindex.csv')
# geosta.to_csv('nonangeosta.csv`')

# data = pd.read_csv('nonanindex.csv', index_col=0)
# (data['pre'].groupby([data.index, data['year']]).sum()/12).to_csv('nonansum.csv')