from netCDF4 import Dataset
import numpy as np
import plotcor
import calculate_corr
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
#col = 'bw'or'color', ranges='all'or'area'
def choose(sy, ey, month, pc, name, hgt=1, csst=1, cuwnd=1, ice=1, col='bw', ranges='all', a=0.01, location='indian'):
    # if hgt == 1:
    #     #读取数据
    #     ncin = Dataset(r'D:\a_postgraduate\changjiang\data\hgt.mon.mean.nc', 'r')
    #
    #     hgt850 = ncin.variables['hgt'][:, 2, :, :]
    #     hgt500 = ncin.variables['hgt'][:, 5, :, :]
    #     hgt250 = ncin.variables['hgt'][:, 8, :, :]
    #     rlons = ncin.variables['lon'][:]
    #     rlats = ncin.variables['lat'][:]
    #     ncin.close()
    #     hgt = [hgt850, hgt500, hgt250]
    #     hgtname = ['hgt850', 'hgt500', 'hgt250']
    #     for j in range(3):
    #         for i in range(2):
    #             # #分割时间
    #             lhgt = calculate_corr.de_time(hgt[j], sy, ey, month, 1948)
    #             #计算回归系数
    #             data, r2, p = calculate_corr.verdata(lhgt, pc[i], a)
    #             # np.savetxt('data0.txt', data)
    #             #分割纬度
    #             slons = rlons
    #             slats = rlats
    #             if ranges == 'area':
    #                 data, lons, lats = calculate_corr.de_ll(data, rlons, rlats, 20, 60, 60, 150)
    #                 r2, lons, lats = calculate_corr.de_ll(r2, rlons, rlats, 20, 60, 60, 150)
    #                 slons = lons
    #                 slats = lats
    #             aa = plotcor.plotcors(data, r2, slons, slats, col=col, ll=hgtname[j], ranges=ranges)
    #             aa.savefig('%s' % (hgtname[j] + '   '+'pc' + str(i+1) + '   ' + name+str(col)))
    #             plt.close()
    if csst == 1:
        #读取数据
        ncin = Dataset(r'D:\a_postgraduate\changjiang\data\sst.mnmean.nc', 'r')
        sst = ncin.variables['sst'][:]
        rlons = ncin.variables['lon'][:]
        rlats = ncin.variables['lat'][:]
        ncin.close()
        sst = calculate_corr.de_time(sst, sy, ey, month, 1854)
        for i in range(2):
            #分割时间
            #计算回归系数
            data, r2, p = calculate_corr.verdata(sst, pc[i], a)
            deldata = data.copy()
            deldata[np.where(p[:, :] > 0.1)] = -999
            r2 = np.ma.masked_equal(deldata, -999.)
            # np.savetxt('data0.txt', data)
            #分割纬度
            slons = rlons
            slats = rlats
            if ranges == 'area':
                if location != 'indian':
                    data, lons, lats = calculate_corr.de_ll(data, rlons, rlats, 40, -40, 30, 300)
                    r2, lons, lats = calculate_corr.de_ll(r2, rlons, rlats, 40, -40, 30, 300)
                else:
                    data, lons, lats = calculate_corr.de_ll(data, rlons, rlats,  40, -40, 50, 130)
                    r2, lons, lats = calculate_corr.de_ll(r2, rlons, rlats,  40, -40, 50, 130)
                slons = lons
                slats = lats
            #绘图
            aa = plotcor.plotcors(data, r2, slons, slats, col=col, ll='sst', ranges=ranges, location=location)
            aa.savefig('%s' % ('sst'  + '   ' +'pc' + str(i+1)+ '   ' + name+str(col)))
            plt.close()
    if cuwnd == 1:
        #读取数据
        ncin = Dataset(r'D:\a_postgraduate\changjiang\data\uwnd.mon.mean.nc', 'r')
        uwnd200 = ncin.variables['uwnd'][:, 9, :, :]
        uwnd850 = ncin.variables['uwnd'][:, 2, :, :]
        uwnd500 = ncin.variables['uwnd'][:, 5, :, :]
        rlons = ncin.variables['lon'][:]
        rlats = ncin.variables['lat'][:]
        ncin.close()
        uwnd = [uwnd850, uwnd500, uwnd200]
        uwndname = ['uwnd850', 'uwnd500', 'uwnd200']
        labels = ['a', 'b']
        for j in range(3):
            # #分割时间
            fig = plt.figure(figsize=[8, 2])#######
            luwnd = calculate_corr.de_time(uwnd[j], sy, ey, month, 1948)
            for i in range(2):
                #计算回归系数
                data, r2, p = calculate_corr.verdata(luwnd, pc[i], a)
                # np.savetxt('data0.txt', data)
                #分割纬度
                slons = rlons
                slats = rlats
                if ranges == 'area':
                    if location != 'indian':
                        data, lons, lats = calculate_corr.de_ll(data, rlons, rlats, 40, -40, 100, 260)
                        r2, lons, lats = calculate_corr.de_ll(r2, rlons, rlats, 40, -40, 100, 260)
                        aa = plotcor.plotcors(data, r2, lons, lats, col=col, ll=uwndname[j], ranges=ranges,
                                              location=location)
                        aa.savefig('%s' % (uwndname[j] + '   ' + 'pc' + str(i + 1) + '   ' + name + str(col)))
                    else:

                        data, lons, lats = calculate_corr.de_ll(data, rlons, rlats, 60, 20, 60, 150)
                        r2, lons, lats = calculate_corr.de_ll(r2, rlons, rlats, 60, 20, 60, 150)
                        aa = plotcor.plotcors(data, r2, lons, lats, col=col, ll=uwndname[j], ranges=ranges,
                                              location=location, axs=plt.subplot(1, 2, i+1, projection=ccrs.PlateCarree(central_longitude=180)), labels=labels[i])

                else:
                    aa = plotcor.plotcors(data, r2, slons, slats, col=col, ll=uwndname[j], ranges=ranges, location=location)
                    aa.savefig('%s' % (uwndname[j] + '   '+'pc' + str(i+1) + '   ' + name+str(col)))
                # plt.close()
            if ranges == 'area':
                if location == 'indian':
                    # plt.show()
                    fig.savefig('%s' % (uwndname[j] + '   ' + 'pc' + '   ' + name + str(col)))

    if ice == 1:
        # 读取数据
        ncin = Dataset(r'D:\a_postgraduate\changjiang\data\HadISST_ice.nc', 'r')
        sic = ncin.variables['sic'][:]
        rlons = ncin.variables['longitude'][:]
        rlats = ncin.variables['latitude'][:]
        ncin.close()
        #分割时间
        sic = calculate_corr.de_time(sic, sy, ey, month, 1870)
        # sic, lons, lats = calculate_corr.de_ll(sic, rlons, rlats, 89.5, 60.5, -179.5, 179.5)
        # print(sic.shape)
        for i in range(2):
            # 计算回归系数
            data, r2, p = calculate_corr.verdata(sic, pc[i], a)
            # 分割纬度
            deldata = data.copy()
            deldata[np.where(p[:, :] > 0.1)] = -999
            deldata = np.ma.masked_equal(deldata, -999.)
            aa = plotcor.plot_northice_basemap(data, deldata, rlons, rlats, col=col)
            aa.savefig('%s' % ('sic' + '   ' + 'pc' + str(i + 1) + '   ' + name + str(col)))
            plt.close()
    return

if __name__ == "__main__":
    month = list(range(1, 13))
    sy = 1980
    ey = 2017

    pc1 = np.loadtxt('pc1.txt')
    pc2 = np.loadtxt('pc2.txt')
    pc = [pc1, pc2]

    a = choose(sy, ey, month, pc,'yes', hgt=0, csst=0, cuwnd=1, ice=0, col='color',ranges='all', a=0.01)

