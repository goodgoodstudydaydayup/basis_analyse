import cartopy.crs as ccrs
import matplotlib.pyplot as plt
from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
import numpy as np
import cartopy as cart
from copy import copy
from cartopy.mpl.gridliner import LATITUDE_FORMATTER, LONGITUDE_FORMATTER
import shapely.geometry as sgeom
from scipy.interpolate import Rbf, RegularGridInterpolator
####绘图时未考虑高度场
def plotcors(data, r2, oslon, oslat, col='bw', ll='0', ranges='all', location='indian', axs=0, labels=0):
    #判断是否为风场印度洋
    if location == 'indian':
        if ll != 'sst':
            ax = axs
        else:
            fig = plt.figure()
            ax = plt.axes(projection=ccrs.PlateCarree(central_longitude=180))
    else:
        if ll != 'sst':
            fig = plt.figure(figsize=[8, 4.3])
            ax = plt.axes(projection=ccrs.PlateCarree(central_longitude=180))
        else:
            fig = plt.figure(figsize=[8, 2.8])
            ax = plt.axes(projection=ccrs.PlateCarree(central_longitude=180))
    colormap = [ '#5884ff',  '#638cfe', '#7c9eff', '#93b0fe', '#a0b8ff', '#abc0ff', '#b6caff', '#c2d3ff', '#ffffff', '#f3ddcf', '#f3d3c4', '#f3c8b8', '#f3b5a0', '#f4a088', '#f3896f', '#f27557', '#f46140','#f46140']
    colormap5 = [ '#ABABAB', '#ffffff',  '#D4D4D4']
    levels = [-1, -.9, -.8, -.7, -.6, -.5, -.4, -.3, -.28, .28, .3, .4, .5, .6, .7, .8, .9, 1]
    level5 = [-1, -0.28, 0.28, 1]

    if ll == 'sst':
        ax.add_feature(cart.feature.LAND, zorder=100, edgecolor='k', facecolor='white')

    if col == 'color':
        delfill = ax.contourf(oslon, oslat, r2, levels, transform=ccrs.PlateCarree(), colors=colormap)
        plt.colorbar(delfill, orientation='horizontal')
        fill = ax.contour(oslon, oslat, data, 5, transform=ccrs.PlateCarree(), colors='k')
        clevels = []
        filllevels = fill.levels
        for i in range(0, len(filllevels), 2):
            clevels.append(filllevels[i])
        ax.clabel(fill, clevels, fmt='%6.1f')
        ax.coastlines()

    if col == 'bw':
        delfill = ax.contourf(oslon, oslat, r2, level5, transform=ccrs.PlateCarree(), colors=colormap5)
        fill = ax.contour(oslon, oslat, data, 10, transform=ccrs.PlateCarree(), colors='k')
        clevels = []
        filllevels = fill.levels
        for i in range(0, len(filllevels), 2):
            clevels.append(filllevels[i])
        ax.clabel(fill, clevels, fmt='%6.1f')
        ax.coastlines()
    # 设置坐标轴经纬度标志
    if ranges == 'area':
        if ll == 'sst':
            if location == 'indian':
                plt.subplots_adjust(top=0.98, bottom=0.08, left=0.01, right=0.99)
                ax.set_xticks(range(int(np.min(oslon)), int(np.max(oslon))+20, 20), crs=ccrs.PlateCarree())
                ax.set_yticks(range(int(np.min(oslat)), int(np.max(oslat))+10, 10), crs=ccrs.PlateCarree())
            else:
                plt.subplots_adjust(left=0.06, bottom=0.08, right=0.98, top=0.97, wspace=0.2, hspace=0.2)
                ax.set_xticks(range(int(np.min(oslon)), int(np.max(oslon)) + 10, 20), crs=ccrs.PlateCarree())
                ax.set_yticks(range(int(np.min(oslat)), int(np.max(oslat)) + 20, 20), crs=ccrs.PlateCarree())
        else:
            if location == 'indian':
            # 匹配印度洋风场
                plt.subplots_adjust(left=0.07, bottom=0.07, right=0.96, top=0.98, wspace=0.2, hspace=0.2)
                ax.set_xticks(range(int(np.min(oslon)), int(np.max(oslon))+10, 20), crs=ccrs.PlateCarree())
                ax.set_yticks(range(int(np.min(oslat)), int(np.max(oslat)) + 10, 10), crs=ccrs.PlateCarree())
                ax.text(int(np.min(oslon)), int(np.max(oslat))+1, labels, transform=ccrs.PlateCarree())
            else:
                plt.subplots_adjust(left=0.06, bottom=0.08, right=0.96, top=0.97, wspace=0.2, hspace=0.2)
                ax.set_xticks(range(int(np.min(oslon)), int(np.max(oslon)) + 20, 20), crs=ccrs.PlateCarree())
                ax.set_yticks(range(int(np.min(oslat)), int(np.max(oslat)) + 20, 20), crs=ccrs.PlateCarree())
    else:
        ax.set_xticks([0, 60, 120, 180, 240, 300, 360], crs=ccrs.PlateCarree())
        ax.set_yticks([-90, -60, -30, 0, 30, 60, 90], crs=ccrs.PlateCarree())
    lon_formatter = LongitudeFormatter(zero_direction_label=True)
    lat_formatter = LatitudeFormatter()
    ax.xaxis.set_major_formatter(lon_formatter)
    ax.yaxis.set_major_formatter(lat_formatter)
    # 添加网格线
    ax.gridlines(color='black', linestyle='--', alpha=0.4)
    # plt.show()
    return fig

def plot_northice_basemap(data, r2, oslon, oslat, col='bw'):
    from mpl_toolkits.basemap import Basemap
    import numpy as np
    import matplotlib.pyplot as plt
    fig = plt.figure(figsize=[8,7.5])
    fig.subplots_adjust(left=0.06, bottom=0.05, right=0.97, top=0.95, wspace=0.20, hspace=0.20)
    oslon, oslat = np.meshgrid(oslon, oslat)
    colormap = [ '#5884ff',  '#638cfe', '#7c9eff', '#93b0fe', '#a0b8ff', '#abc0ff', '#b6caff', '#c2d3ff', '#ffffff', '#f3ddcf', '#f3d3c4', '#f3c8b8', '#f3b5a0', '#f4a088', '#f3896f', '#f27557', '#f46140','#f46140']
    levels = [-1, -.9, -.8, -.7, -.6, -.5, -.4, -.3, -.28, .28, .3, .4, .5, .6, .7, .8, .9, 1]

    colormap = [ '#5884ff',  '#638cfe', '#7c9eff', '#93b0fe', '#a0b8ff', '#abc0ff', '#b6caff', '#c2d3ff', '#ffffff', '#f3ddcf', '#f3d3c4', '#f3c8b8', '#f3b5a0', '#f4a088', '#f3896f', '#f27557', '#f46140','#f46140']
    colormap5 = [ '#ABABAB', '#ffffff',  '#D4D4D4']
    levels = [-1, -.9, -.8, -.7, -.6, -.5, -.4, -.3, -.1, .1, .3, .4, .5, .6, .7, .8, .9, 1]
    level5 = [-1, -0.01, 0.01, 1]

    m = Basemap(projection='nplaea',boundinglat=60, lon_0=270, resolution='l')
    m.drawcoastlines(linewidth=0.5)
    m.fillcontinents()
    # draw parallels and meridians.
    m.drawparallels(np.arange(-80., 81., 10.))
    m.drawmeridians(np.arange(-180., 181., 30.), labels=[True, True, True, True])
    # m.drawmapboundary(fill_color=None)
    # draw tissot's indicatrix to show distortion.
    ax = plt.gca()
    oslon, oslat = m(oslon, oslat)

    if col == 'color':
        cs = m.contourf(oslon, oslat, r2, levels, colors=colormap)
        cbar = m.colorbar(cs, location='bottom', pad="5%")

    if col == 'bw':
        cs = m.contour(oslon, oslat, data, 3, colors='k', linewidths=1)
        delfill = m.contourf(oslon, oslat, r2, levels, colors=colormap)
        plt.clabel(cs)
    # plt.show()
    return fig
