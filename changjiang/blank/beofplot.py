import cartopy.crs as ccrs
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import Rbf
from matplotlib.path import Path
from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
from matplotlib.patches import PathPatch
from shapely.geometry import Polygon as ShapelyPolygon
from shapely.geometry import Point as ShapelyPoint
from scipy.stats.mstats import zscore


def ploteofs(ax, data, lon, lat, labela, color, shps=None):

    # 插值初定义
    olon = np.linspace(int(min(lon)), int(max(lon)), 88)
    olat = np.linspace(int(min(lat)), int(max(lat)), 88)

    olon,olat = np.meshgrid(olon, olat)
    # 插值处理
    func = Rbf(lon, lat, data, function='linear')
    data_new = func(olon, olat)

    #绘制contour
    # ax = plt.axes(projection=ccrs.PlateCarree(central_longitude=102))
    #建立坐标系，并将裁剪的path得出
    if shps != None:
        plate_carre_data_transform = ccrs.PlateCarree()._as_mpl_transform(ax)
        plat = shps[:, 1]
        plon = shps[:, 0]
        upath = Path(list(zip(plon, plat)))
        upath = PathPatch(upath, transform=plate_carre_data_transform)
        line= ax.plot(plon, plat, transform=plate_carre_data_transform, color='k', alpha=0.7)

        fill = ax.contour(olon, olat, data_new, 10, transform=ccrs.PlateCarree(), colors='k', alpha=0.8)

        for collection in fill.collections:
            collection.set_clip_path(upath)
        CS_label = ax.clabel(fill, fmt='%6.1f')

        # 删除clabel
        aaa = ccrs.PlateCarree(central_longitude=102).transform_points(ccrs.Geodetic(), plon, plat)
        clip_map_shapely = ShapelyPolygon(aaa)
        for text_object in CS_label:
            if not clip_map_shapely.contains(ShapelyPoint(text_object.get_position())):
                text_object.set_visible(False)

    fill = ax.contour(olon, olat, data_new, 10, transform=ccrs.PlateCarree(), colors='k', alpha=0.8)
    if color == 'bw':
        fill = ax.contourf(olon, olat, data_new, 10, transform=ccrs.PlateCarree(), cmap=plt.cm.gray, alpha=0.4)
    if color == 'color':
        fill = ax.contourf(olon, olat, data_new, 10, transform=ccrs.PlateCarree(), cmap=plt.cm.RdBu_r)
        plt.colorbar(fill, orientation='horizontal')

    if shps != None:
        for collection in fill.collections:
            collection.set_clip_path(upath)

    # #添加shp文件进入图片中

    # fname = r'bianjie.shp'
    # shape_feature = ShapelyFeature(Reader(fname).geometries(), ccrs.PlateCarree(), edgecolor='k', facecolor='none', linewidths=0.55)
    # ax.add_feature(shape_feature)

    #设置坐标轴经纬度标志
    ax.set_xticks(range(int(np.min(olon)), int(np.max(olon)), int((np.max(olon)-np.min(olon))/3)), crs=ccrs.PlateCarree())
    print(np.max(olat), np.min(olat))
    ax.set_yticks(range(int(np.min(olat)), int(np.max(olat)), int((np.max(olat)-np.min(olat))/2)), crs=ccrs.PlateCarree())
    lon_formatter = LongitudeFormatter(zero_direction_label=True)
    lat_formatter = LatitudeFormatter()
    ax.xaxis.set_major_formatter(lon_formatter)
    ax.yaxis.set_major_formatter(lat_formatter)
    plt.text(int(np.min(olat)), int(np.max(olon)), labela, transform=ccrs.PlateCarree())
    ax.tick_params(labelsize=8)

    return plt

def plotpc(ax, pc, sy, ey, labela):
    ma = fast_moving_average(pc, 11)
    years = range(sy, ey + 1)
    ax.bar(years, pc, color='black')
    plt.plot(years, ma, color='k', linewidth=1.5)

    ax.axhline(0, color='k')
    ax.axhline(1, color='k', linestyle='--', alpha=0.5)
    ax.axhline(-1, color='k', linestyle='--', alpha=0.5)
    ax.set_xticks(range(sy, ey + 1, int((ey-sy)/5)))
    # ax.set_yticks([-2, -1, 0, 1, 2])
    plt.xlim(sy, ey)
    plt.ylim(-3, 3)

    plt.text(sy + 1, 0.1, labela)
    ax.tick_params(labelsize=8)
    return plt


def fast_moving_average(x, N):
    return np.convolve(x, np.ones((N,)) / N)[(N - 1):]

def nor(data):
    max = np.max(data)
    min = np.min(data)
    cdata = []
    for i in data:
        cdata.append((i-min)/(max-min))
    return cdata

def plotline(ax, pc, sy, ey, labela, nors=None):
    print(pc.shape)
    if nors != None:
        pc = nor(pc)
        plt.ylim(0, 1.2)
    ma = fast_moving_average(pc, 11)
    years = range(sy, ey + 1)
    ax.plot(years, pc, color='black')
    ax.axhline(0, color='k')
    ax.set_xticks(range(sy, ey + 1, int((ey-sy)/5)))
    # ax.set_yticks([-2, -1, 0, 1, 2])
    plt.xlim(sy, ey)
    plt.text(sy, 1.1, labela)
    ax.tick_params(labelsize=8)
    return plt