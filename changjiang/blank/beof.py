import numpy as np
import pandas as pd
import beofplot
from eofs.standard import Eof
import matplotlib.pyplot as plt
import cartopy.crs as ccrs


#shp为边界文件，lon,lat
#data:每个站点的年雨量序列,站点名为index,colunms为‘pre','year'
#geosta:每个站点的经纬度，要求站点名为index,colunms为‘lat’,'lon',用于白化。
#neof:需要几个eof导出
def eoff(sy, ey, data, geosta, neof ,color, name, shps=None):
    # 剔除无效站点
    nanindex = (data[np.isnan(data['pre'])].index).unique()
    for i in nanindex:
        data = data.drop(i)
        geosta = geosta.drop(i)
    lenstation = len(geosta.index)
    n = ey + 1 - sy
    pre = np.empty([n, lenstation])

    #提取雨量值
    for i, year in enumerate(range(sy, ey+1)):
        pre[i, :] = data[data['year'] == year]['pre']
    solver = Eof(pre)

    eof = solver.eofsAsCorrelation(neofs=neof)
    pc = solver.pcs(npcs=neof, pcscaling=1)
    np.savetxt('pc1.txt', pc[:, 0])
    np.savetxt('pc2.txt', pc[:, 1])
    print(solver.varianceFraction(neigs=4))
    lisss = ['a', 'c', 'b', 'd']
    # #
    fig = plt.figure()
    grid = plt.GridSpec(11, 88, wspace=0.5, hspace=0.5)
    for i in range(2):
        peof = beofplot.ploteofs(plt.subplot(grid[i*5+i:i*5+5+i, 0:53], projection=ccrs.PlateCarree(central_longitude=102)), eof[i], np.array(geosta['lon']), np.array(geosta['lat']), lisss[i], color, shps)
        ppc = beofplot.plotpc(plt.subplot(grid[i*5+i:i*5+5+i, 59:88]), pc[:, i], sy, ey, lisss[i+2])
    plt.savefig('eof%s.png' % (name+str(color)), dpi=1200)
    return

if __name__ == "__main__":
    # data = pd.read_csv('year_sum.csv', index_col=[0])
    # geosta = pd.read_csv('geostas.csv',index_col=[0])
    # a = eoff(1985, 2016, data,geosta, 1, 198)
    data = pd.read_csv('nonansum.csv', index_col=[0])
    geosta = pd.read_csv('nonangeosta.csv', index_col=[0])
    shps = np.loadtxt(r'D:\a_postgraduate\changjiang\data\shp.txt', usecols=[1, 2])
    a = eoff(1980, 2016, data,geosta,shps, 2, 'color','yes')

