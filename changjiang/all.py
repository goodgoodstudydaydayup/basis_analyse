import numpy as np
import pandas as pd
from pandas import DataFrame
import sys
sys.path.append(r'D:\basis\changjiang\blank')
sys.path.append(r'D:\basis\changjiang\cor')
from blank import beof
from area_cor import choose
from scipy.interpolate import griddata

geosta = pd.read_csv(r'D:\a_postgraduate\changjiang\data\station_with_ll.csv', index_col=[0])
shps = np.loadtxt(r'D:\a_postgraduate\changjiang\data\shp.txt', usecols=[1, 2])

month = [[4, 5], [6, 7, 8], [9, 10], [11, 12, 1, 2, 3]]
month1 = [[1, 2, 3], [3, 4, 5], [8, 9, 10]]
name = ['汛前', '夏汛', '秋汛', '枯期']
for i in range(1, 5):
    data = pd.read_csv(r'D:\a_postgraduate\changjiang\data\data%i.csv'%i, index_col=0)

    a = beof.eoff(1980, 2017, data, geosta, shps, 2, 'color', name[i-1])

    # pc1 = np.loadtxt('pc1.txt')
    # pc2 = np.loadtxt('pc2.txt')
    # pc = [pc1, pc2]
    # b = choose(1980, 2017, month[i-1], pc, name[i-1], hgt=0, csst=1, cuwnd=0, ice=0, col='color', ranges='area', a=0.1, location='paci')

# data = pd.read_csv(r'D:\a_postgraduate\changjiang\data\data1.csv', index_col=0)
#
# a = beof.eoff(1983, 2017, data, geosta, shps, 4, 'color', '1')
# # pc1 = np.loadtxt('pc1.txt')
# # pc2 = np.loadtxt('pc2.txt')
# # pc = [pc1, pc2]
# # b = choose(1983, 2017, range(1,13), pc, 'no', csst=1, cuwnd=0, col='bw', ranges='area', a=0.1)


