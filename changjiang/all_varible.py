import sys
import numpy as np
import pandas as pd
sys.path.append(r'D:\basis\changjiang\blank')
sys.path.append(r'D:\basis\changjiang\cor')
sys.path.append(r'D:\basis\changjiang\dedata')
import matplotlib.pyplot as plt
from blank import beof, beofplot
from cor import area_cor
from dedata import datafindnan, fist_ana_data, ba, MK_mutation, reggression
from pylab import mpl
mpl.rcParams['font.sans-serif'] = ['SimHei']
mpl.rcParams['axes.unicode_minus'] = False

def delspace(dataname, areaname, datanumber, resetunit):
    rowdata = pd.read_csv(r'D:\basis\changjiang\data\%s.txt' % dataname, delim_whitespace=True, header=None, usecols=[0,4,5,6,datanumber]
                       , index_col=0, names=['station', 'year', 'month', 'day', 'pre'])
    for staname in areaname:
        areastation = pd.read_csv(r'D:\basis\changjiang\data\%s.csv' % staname, index_col=0)
        data = fist_ana_data.fchoose(rowdata.loc[areastation.index], sy, ey, resetunit)
        area, station = datafindnan.findnan(data, np.array(areastation.index), areastation, delnan=None)
        # area['pre'][np.isnan(area['pre'])] = 0
        # area['pre'] = np.ma.masked_equal(area['pre'], 0)
        area.to_csv('%s.csv' % (staname+dataname))
        station.to_csv('%s sta.csv' % (staname+dataname))
    return


def basis_space_analysis(data, sta, dataname, staname, nor=None):
    year_ave = data['pre'].groupby(data['year']).mean()
    month_ave = data['pre'].groupby(data['month']).mean()
    basis = ba.basis_analysis(data, ey+1-sy, len(sta))
    basis.to_csv('result%s.csv' % (staname+dataname))
    #plot
    fig = plt.figure(figsize=[12, 8])
    a = beofplot.plotline(plt.subplot(2, 1, 1), np.array(year_ave).flatten(), sy, ey, '年平均', nor)
    b = beofplot.plotline(plt.subplot(2, 1, 2), np.array(month_ave).flatten(), 1, 12, '月平均', nor)
    plt.savefig('平均计算%s.png' % (staname+dataname))
    return


def deltime(rowdata, month, dataname, staname,  smonth=None, emonth=None, sy=None):
    for number, i in enumerate(month):
        n = len(i)
        if n == 2:
            data = rowdata[(rowdata['month'] == i[0]) | (rowdata['month'] == i[1])]
        elif n == 3:
            data = rowdata[(rowdata['month'] == i[0]) | (rowdata['month'] == i[1]) | (rowdata['month'] == i[2])]
        elif n == 4:
            data = rowdata[(rowdata['month'] == i[0]) | (rowdata['month'] == i[1]) | (rowdata['month'] == i[2]) | (rowdata['month'] == i[3])]
        elif n == 5:
            data = rowdata[(rowdata['month'] == i[0]) | (rowdata['month'] == i[1]) | (rowdata['month'] == i[2]) | (rowdata['month'] == i[3]) | (rowdata['month'] == i[4])]
        else:
            data = None
            print('month is longer than 5, please check or change your program')
        (data['pre'].groupby([data.index, data['year']]).sum()/n).to_csv('data%s.csv' % (str(number)+staname+dataname))
        # a = (data['pre'].groupby([data.index, data['year']]).sum() / n).reset_index()
        # print(a['level_0'].unique().shape)
    if smonth != None:
        data = rowdata[(rowdata['month'] == 1) | (rowdata['month'] == 2)
                       | (rowdata['month'] == 3) | (rowdata['month'] == 11)]
        data['year'][(data['month'] == 1) | (data['month'] == 2) | (data['month'] == 3)] = np.array(
            data['year'][(data['month'] == 1) | (data['month'] == 2) | (data['month'] == 3)].apply(lambda x: x - 1))

        data = data[(data['month'] == 1) | (data['month'] == 2) | (data['month'] == 3) | (data['month'] == 11) | (
                    data['month'] == 12)]
        data = data[(data['year'] != sy-1)]
        data['pre'].groupby([data.index, data['year']]).sum().to_csv('data%s.csv' % (staname+dataname))
    return


def eofacor(data, geosta, month, name, stagename, col, location, area, hgt=0, csst=1, cuwnd=0, ice=0, shps=None):
    a = beof.eoff(sy, ey, data, geosta, 2, 'color', name+stagename, shps=shps)

    pc1 = np.loadtxt('pc1.txt')
    pc2 = np.loadtxt('pc2.txt')
    pc = [pc1, pc2]
    b = area_cor.choose(sy, ey, month, pc, name+stagename, hgt=hgt, csst=csst, cuwnd=cuwnd, ice=ice, col=col, ranges=area,
               a=0.1, location=location)
    return


def single_year(data, month, sy1, dataname, staname):
    fig = plt.figure()
    # a = MK_mutation.mkmu(plt.subplot(2, 2, 1),data, sy, ey)
    b = reggression.cal_wl(fig, plt.subplot(2, 2, 1), plt.subplot(2, 2, 2), data, sy, ey, month)
    c = reggression.regg(plt.subplot(2, 2, 3), list(range(sy, ey+1)), data, sy1-sy, month)
    plt.savefig('检验%s.png' % (staname+dataname))
    return


def all(dataname, datanumber, resetunit, month, monthname):
    area = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
    # area = ['a', 'b']
    delspace(dataname, area, datanumber, resetunit)
    for staname in area:
        data = pd.read_csv('%s.csv' % (staname+dataname))
        sta = pd.read_csv('%s sta.csv' % (staname+dataname), index_col=0)
        a = basis_space_analysis(data, sta, dataname, staname, nor=1)
        data = pd.read_csv('%s.csv' % (staname+dataname), index_col=0)
        a = deltime(data, month, dataname, staname)
        for i, m in enumerate(month):
            data = pd.read_csv('data%s.csv' % (str(i)+staname+dataname), index_col=0, header=None, names=['year', 'pre'])
            a = eofacor(data, sta, m, monthname[i], (str(i)+staname+dataname), 'color', 'all', 'paci', hgt=0, csst=1, cuwnd=0, ice=0)
            data = data['pre'].groupby(data['year']).mean()
            a = single_year(np.array(data), '全年%s' % (str(i)+staname+dataname), 2000, dataname, staname)
    return

sy = 1980
ey = 2016
month = [[4, 5, 6], [7, 8, 9]]
monthname = ['夏汛', '秋汛']
all('SSD', 7, 1, month, monthname)




