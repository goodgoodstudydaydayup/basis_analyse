import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from scipy.stats import linregress
from scipy import signal
from pylab import mpl

mpl.rcParams['font.sans-serif'] = ['SimHei']
mpl.rcParams['axes.unicode_minus'] = False


#x是时间，年份数据； y是年份对应的降雨量
def regg(ax, time, y, time2, month):
    beta_coeff, intercept, rvalue, pvalue, stderr = fit_line(time, y)
    beta_coeff90, intercept90, rvalue90, pvalue90, stderr90 = fit_line(time[time2:], y)
    plotall(ax, x, x2, a, month, beta_coeff, intercept, beta_coeff90
            , intercept90, rvalue, pvalue, rvalue90, pvalue90)

    return


def fit_line(x, y):
    (beta_coeff, intercept, rvalue, pvalue, stderr) = linregress(x, y)
    return beta_coeff, intercept, rvalue, pvalue, stderr

def plotall(ax, time, time2, line1, month, beta_coeff, intercept,beta_coeff00, intercept00, r, p, r00, p00):
    line2 = time * beta_coeff +intercept
    line3 = time2 * beta_coeff00 +intercept00
    ax.set_ylim(min(min(line1), min(line2)), max(line1)+((max(line1)-min(line1))/3))
    plt.plot(time, line1, label='观测', color='k', marker='o')
    plt.plot(time, line2, label='趋势', color='red')
    plt.plot(time2, line3, label='00后趋势', color='blue', linestyle='--')
    ax.text(min(time)-1, max(line1)+(8/10)*((max(line1)-min(line1))/3), '%s的趋势方程为:y = %.2fx+%.2f' % (month, beta_coeff, intercept),fontsize=8)
    ax.text(min(time)-1, max(line1)+(6/10)*((max(line1)-min(line1))/3), 'R-value 为 %.3f ,P-value 为 %.3f' % (r,p),fontsize=8)
    ax.text(min(time)-1, max(line1)+(4/10)*((max(line1)-min(line1))/3), '00后的趋势方程为:y = %.2fx+%.2f' % (beta_coeff00, intercept00),fontsize=8)
    ax.text(min(time)-1, max(line1)+(2/10)*((max(line1)-min(line1))/3), 'R-value 为 %.3f ,P-value 为 %.3f' % (r00, p00),fontsize=8)
    plt.legend(loc=1, fontsize=6)
    ax.set_ylabel('降水/mm')
    ax.set_xlabel('年')
    return


def cal_wl(fig, ax1, ax2, y_data, sy, ey, month):      #####小波分析
    n = y_data.size
    widths = np.arange(1, n/6)
    cwtmatr = signal.cwt(y_data, signal.ricker, widths)   ##用的是ricker函数，宽度为数据的六分之一
    var = []
    for i in range(n):
        var.append(sum(np.square(cwtmatr[:, i])))
    print(cwtmatr.shape)
    a = view_wl(fig, ax1, cwtmatr, sy, ey, n, month)
    b = view_vari(ax2, var, sy, ey, month)
    return cwtmatr, var

def view_wl(fig, ax, cwtmatr, y0, y1, n, name):
    cs1 = ax.contourf(cwtmatr, extent=[y0, y1, 1, n/2], cmap='rainbow',
           aspect='auto', vmax=abs(cwtmatr).max(), vmin=-abs(cwtmatr).max())
    cs2 = ax.contour(cs1, cwtmatr, extent=[y0, y1, 1, n/2], colors='black',
           aspect='auto', vmax=abs(cwtmatr).max(), vmin=-abs(cwtmatr).max())
    ax.text(y0+1, 0.6, name)
    ax.set_ylabel(u'周期/年')
    ax.set_xlabel(u'年份')
    ax.invert_yaxis()
    ax.set_xticks([1980, 1990, 2000, 2010, 2017])
    fig.colorbar(cs1)
    return ax

def view_vari(ax, var, y0, y1, name):
    pvar = ax.plot(range(y0, y1 + 1), var, color='black')
    ax.set_xlabel(u'时间尺度/a')
    ax.text(y0, ((max(var)-min(var))*21.8/20), name)
    ax.set_xticks([1980, 1990, 2000, 2010, 2017])
    ax.set_xticklabels(['0', '10', '20', '30', '37'])
    # ax.set_xlabel(['1980','1990','2010','2017'])
    return


if __name__ == "__main__":
    fig1 = plt.figure(figsize=[10, 13])
    fig1.subplots_adjust(left=0.05, bottom=0.05, right=0.99, top=0.99, wspace=0.22, hspace=0.24)
    month = range(1, 13)
    data = pd.read_csv(r'D:\a_postgraduate\changjiang\data\data.csv', index_col=0)
    x = list(range(1980, 2018))
    x2 = list(range(2000, 2018))
    sum = data['pre'].groupby([data['month'], data['year']]).sum()/78
    for i in range(1, 13):
        a = np.array(sum[i])
        beta_coeff, intercept, rvalue, pvalue, stderr = fit_line(x, a)
        beta_coeff90, intercept90, rvalue90, pvalue90, stderr90 = fit_line(x2, a[20:])
        ccc = plotall(fig1.add_subplot(4, 3, i), x ,x2, a, '%s月'%month[i-1], beta_coeff, intercept,beta_coeff90, intercept90, rvalue, pvalue, rvalue90, pvalue90)
    fig1.savefig('reggaa.png', dpi=1500)
    #
    # fig2 = plt.figure(figsize=[9,9])
    # fig2.subplots_adjust(left=0.06, bottom=0.06, right=0.99, top=0.99, wspace=0.15, hspace=0.15)
    # # ysum = data['pre'].groupby(data['year']).sum()/78/38
    # # a = np.array(ysum)
    # # beta_coeff, intercept, rvalue, pvalue, stderr = fit_line(x, a)
    # # beta_coeff90, intercept90, rvalue90, pvalue90, stderr90 = fit_line(x2, a[20:])
    # # a = plots(fig2.add_subplot(2, 2, 1), x ,x2, a, '年平均降水量',beta_coeff, intercept,beta_coeff90, intercept90, rvalue, pvalue, rvalue90, pvalue90)
    #
    # #汛期计算##############################################################
    # data['month'][(data['month'] == 4) | (data['month'] == 5)] = 44
    # data['month'][(data['month'] == 6) | (data['month'] == 7) | (data['month'] == 8)] = 88
    # data['month'][(data['month'] == 9) | (data['month'] == 10)] = 99
    # sum = data['pre'].groupby([data['month'], data['year']]).sum()/78
    # month = ['汛前', '夏汛', '秋汛']
    # for i,m in enumerate([44, 88, 99]):
    #     a = np.array(sum[m])
    #     beta_coeff, intercept, rvalue, pvalue, stderr = fit_line(x, a)
    #     beta_coeff90, intercept90, rvalue90, pvalue90, stderr90 = fit_line(x2, a[20:])
    #     a = plots(fig2.add_subplot(2, 2, i+1), x ,x2, a, month[i], beta_coeff, intercept,beta_coeff90, intercept90, rvalue, pvalue, rvalue90, pvalue90)
    # #
    # # #枯期计算##############################################################
    # data = pd.read_csv(r'D:\a_postgraduate\changjiang\data\data3.csv', index_col=0)
    # # data['year'][(data['month'] == 1) | (data['month'] == 2) | (data['month'] == 3)] = np.array(data['year'][(data['month'] == 1) | (data['month'] == 2) | (data['month'] == 3)].apply(lambda x: x-1))
    #
    # sum = data['pre'].groupby(data['year']).sum()/78/38
    # x = range(1980, 2018)
    # x2 = range(2000, 2018)
    # a = np.array(sum)
    # beta_coeff, intercept, rvalue, pvalue, stderr = fit_line(x, a)
    # beta_coeff90, intercept90, rvalue90, pvalue90, stderr90 = fit_line(x2, a[20:])
    # a = plots(fig2.add_subplot(2, 2, 4), x, x2, a, '枯期', beta_coeff, intercept, beta_coeff90, intercept90, rvalue, pvalue, rvalue90, pvalue90)
    # fig2.savefig('reggs.png', dpi=2000)




    # fig1 = plt.figure()
    # fig2 = plt.figure()
    # fig1.subplots_adjust(left=0.08, bottom=0.11, right=0.97, top=0.92, wspace=0.15, hspace=0.39)
    # fig2.subplots_adjust(left=0.09, bottom=0.10, right=0.99, top=0.96, wspace=0.25, hspace=0.36)
    # name = ['汛前', '夏汛', '秋汛', '枯期']
    # for i in range(1, 5):
    #     data = pd.read_csv(r'D:\a_postgraduate\changjiang\data\data%i.csv'%i, index_col=0)
    #     y_data = np.array(data['pre'].groupby(data['year']).sum()/78)
    #     n = len(y_data)
    #     cwtmatr, var = cal_wl(y_data)
    #     a = view_wl(fig1.add_subplot(2, 2, i), cwtmatr, 1980, 2017, n, name[i-1])
    #     b = view_vari(fig2.add_subplot(2, 2, i), var, 1980, 2017, name[i-1])

    # fig1.savefig('cwtmart.png', dpi=1200)
    # fig2.savefig('vari.png', dpi=1200)
    #
    # fig1 = plt.figure(figsize=[9,10])
    # fig2 = plt.figure(figsize=[9,10])
    # fig1.subplots_adjust(left=0.06, bottom=0.06, right=0.98, top=0.98, wspace=0.27, hspace=0.35)
    # fig2.subplots_adjust(left=0.06, bottom=0.06, right=0.99, top=0.98, wspace=0.2, hspace=0.36)
    # data = pd.read_csv(r'D:\a_postgraduate\changjiang\data\data0.csv', index_col=0)
    # y_datas = data['pre'].groupby([data['month'], data['year']]).sum()/78
    # for i in range(1, 13):
    #     y_data = np.array(y_datas[i])
    #     n = len(y_data)
    #     cwtmatr, var = cal_wl(y_data)
    #     a = view_wl(fig1.add_subplot(4, 3, i), cwtmatr, 1980, 2017, n, '%s月'%i)
    #     b = view_vari(fig2.add_subplot(4, 3, i), var, 1980, 2017, '%s月'%i)
    # fig1.savefig('cwtmartall.png', dpi=1200)
    # fig2.savefig('variall.png', dpi=1200)


