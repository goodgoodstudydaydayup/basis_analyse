import numpy as np
import pandas as pd
from pandas import DataFrame,Series

#data所属结构需要为dataframe，其中有降雨量以及对应的月份，colunmns为：‘pre’,'month'
def basis_analysis(data, lenyear, lensta):
    result = {}
    dfresult = DataFrame()
    mean = data['pre'].groupby(data['month']).sum()/lenyear/lensta
    contribute_rate = (data['pre'].groupby(data['month']).sum())*100/(data['pre'].sum())
    max = data['pre'].groupby(data['month']).idxmax()
    max_pre = data['pre'][max]
    max_year = data['year'][max]
    # data[data['pre'] == 0] = 999
    min = data['pre'].groupby(data['month']).idxmin()
    min_pre = data['pre'][min]
    min_year = data['year'][min]

    data = data.groupby([data['year'], data['month'], data['day']]).mean()
    data = data.reset_index()
    std = data['pre'].groupby(data['month']).std()
    ske = data['pre'].groupby(data['month']).skew()
    cv = std/mean


    dfresult['mean'] = mean
    dfresult['contribute_rate'] = contribute_rate
    dfresult['std'] = std
    dfresult['skew'] = ske
    dfresult['cv'] = cv
    # dfresult['max_pre'] = np.array(max_pre)

    dfresult['max_year'] = np.array(max_year)

    # dfresult['min_pre'] = np.array(min_pre)
    # dfresult['min_year'] = np.array(min_year)
    return dfresult


if __name__ == "__main__":
    '''默认显著性水平为0.05'''
    # data = pd.read_csv(r'D:\a_postgraduate\changjiang\data\data.csv')
    # # data['year'][(data['month'] == 1) | (data['month'] == 2) | (data['month'] == 3)] = np.array(data['year'][(data['month'] == 1) | (data['month'] == 2) | (data['month'] == 3)].apply(lambda x: x-1))
    # #
    # # data['month'] = 11
    # data['month'][(data['month'] == 9) | (data['month'] == 10)] = 44
    # # data['month'][(data['month'] == 6) | (data['month'] == 7) | (data['month'] == 8)] = 88
    # # data['month'][(data['month'] == 1) | (data['month'] == 2) | (data['month'] == 3) | (data['month'] == 12) | (data['month'] == 11)] = 99
    #
    # a = basis_analysis(data)
    data = pd.read_csv(r'D:\a_postgraduate\changjiang\data\data.csv', index_col=0)
    data1 = data[(data['month'] == 9) | (data['month'] == 10)]
    data1['pre'].groupby([data1.index, data1['year']]).sum().to_csv('data0.csv')
