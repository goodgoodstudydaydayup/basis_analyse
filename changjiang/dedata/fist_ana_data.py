import numpy as np
import pandas as pd
from pandas import DataFrame

def fchoose(data, sy, ey, resetunit):
    rdata = choose_year(data, sy, ey)
    rdata['pre'] = stands(np.array(rdata['pre']), resetunit)
    return rdata
#异常数据清除
#data为narray格式
def stands(data, resetunit):
    index = np.where(data == 32766)
    data[index] = -100000
    index = np.where(data > 32000)
    data[index] = 1
    index = np.where(data > 31000)
    data[index] = data[index] - 31000
    index = np.where(data > 30000)
    data[index] = data[index] - 30000
    data = data/resetunit
    data[np.where(data < -500)] = np.nan
    return data
#找出所需要的站点
#data为dataframe格式，要求站点一列为['station']
def choose_stations(data, stations):
    data = data.loc[stations]
    return data

# 找出所需要的年份
#data为dataframe格式，要求年份一列为['year']
def choose_year(data, sy, ey):
    data = data[(data['year'] >= sy) & (data['year'] <= ey)]
    return data

def data_range(data, month):
    data[(data['month'] == 11) | (data['month'] == 12) | (data['month'] == 1) | (data['month'] == 2) | (
                data['month'] == 3)].to_csv('data3.csv')
    data[(data['month'] == 6) | (data['month'] == 7) | (data['month'] == 8)].to_csv('data2.csv')
    data[(data['month'] == 4) | (data['month'] == 5)].to_csv('data1.csv')
    return


if __name__ == "__main__":
    rowdata = pd.read_csv(r'D:\a_postgraduate\changjiang\data\PRE.txt', delim_whitespace=True, header=None, index_col=0, usecols=[0,1,2,4,5,6,7], names=['station','lat','lon','year','month','day','pre'])
    # # rowdata = pd.read_csv(r'D:\a_postgraduate\changjiang\data\SURF_CLI_CHN_MUL_DAY-PRE-13011-201801.txt', delim_whitespace=True, header=None, index_col=0, usecols=[0,4,5,6,7], names=['station','year','month','day','pre'])
    # station = pd.read_csv(r'geoshp.csv', usecols=[0], skiprows=[0], header=None)
    # data = choose_stations(rowdata, np.array(station).flatten())
    # #
    # # print(rowdata)
    # data = choose_year(data, 1990, 2016)
    # print(len(data.index.unique()))
    # data['pre'] = findnan(np.array(data['pre']))
    # print(len(data.index.unique()))
    # # data = data.fillna(-999)
    # # data = data.groupby([data.index, data['year']]).sum()
    # # data[data == -999] = np.nan
    # sdata = (data['pre'].groupby([data.index, data['year']]).sum())/12
    # sdata.to_csv('data0.csv')


    # rowdata = pd.read_csv(r'D:\a_postgraduate\changjiang\data\data3.csv', index_col=0)
    # rowdata[rowdata==0] = np.nan
    # rowdata.to_csv('data3.csv')

    # data = pd.read_csv(r'resultdata.csv', index_col=0)
    # data = data[(data['year'] != 1983) & ((data['month'] == 6) | (data['month'] == 7) | (data['month'] == 8))]
    # # data = data[data['year'] != 1983]
    # # data = data[(data['year'] != 1983) & ((data['month'] == 4) | (data['month'] == 5))]
    # ((data['pre'].groupby([data.index, data['year']]).sum())).to_csv('data000.csv')

    # rowdata = pd.read_csv(r'D:\a_postgraduate\changjiang\data\PRE.txt', delim_whitespace=True,
    #                       header=None, index_col=0, usecols=[0,4,5,6,7],
    #                       names=['station', 'year', 'month', 'day', 'pre'])
    # data = choose_year(rowdata, 1980, 2016)
    # data['pre'] = findnan(np.array(data['pre']))
    # data.to_csv('rowdata.csv')
    # data = (data['pre'].groupby([data.index, data['year']]).sum())/12
    # data.to_csv('data.csv')

    # data = pd.read_csv('data.csv', index_col=0)
    # dataindex = data.index.unique()
    # geodata = pd.read_csv('rowdata.csv', index_col=0, usecols=[0])
    # geodata = geodata[~geodata.index.duplicated(keep='first')]
    # # geodata = pd.read_csv('geoshp.csv',index_col=0)
    # # geodata = geodata.reindex(dataindex)
    # geodata.to_csv('geoshp11.csv')



